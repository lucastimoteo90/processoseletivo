# Conteúdo
<!--ts-->
* [Sobre](#sobre)
* [Demo](#demonstração)
* [Instalação](#instalação)
* [Como usar](#como-usar)
* [Arquitetura](#arquitetura)
<!--te-->

# Sobre


# Demonstração

Acesso do cliente(Retirada de senha)
http://34.69.45.8/

Acesso do gerente
http://34.69.45.8/gerente

Acesso painel(TV por exemplo)
http://34.69.45.8/painel


# Instalação
Efetuar o clone do repositório https://gitlab.com/lucastimoteo90/processoseletivo

## Instalação do backend-servidor(Spring Boot).
###Pré-requisito
1. Java 11

###Passo a passo
Entrar no diretório **backend**

`cd backend`

Instalar dependências maven:

`./mvnw install`

Para executar o servidor localmente:

`./mvnw spring-boot:run`

## Instalação do cliente web(Ionic)
### Pré-requisito
1. Gerenciador de pacotes NPM instalado.

###Passo a passo
Instalar ionic-cli: 

`npm install -g @ionic/cli`

Entrar no diretório **app**:

`cd app`

Instalar dependências:

`npm install`

#### Execução
Para testar localmente, dentro do diretório **app** executar o comando:

`ionic serve`

_Obs: Para executar localmente é preciso apontar o endereço do servidor(backend) no arquivo **src/environments/environment.ts**. por padrão o endereço configurado é http://localhost:8080/_

Para gerar uma versão compilada(produção) executar o comando:

`ionic build --prod`

_Obs: Para compilar versão de produção é preciso apontar o endereço do servidor(backend) no arquivo **src/environments/environment.prod.ts**._

Com isso é criado dentro do diretório app/www os arquivos da aplicação.


# Como usar

O sistema possui três interfaces:

1.  Interface CLIENTE, retirada e acompanhamento de senhas.
    ![](./man/FluxoInterface1.png)
2. Interface GERENTE, administração da fila de atendimentos (senhas).
   ![](./man/FluxoGerente.png)
3. Interface PAINEL, interface para TV com acompanhamento das senhas.
   ![](./man/Painel.png)


# Arquitetura 

## Arquitetura do software:

![](./man/Arquitetura.png)

## Fluxo de status das senhas:

![](./man/FluxoSenhas.png)


