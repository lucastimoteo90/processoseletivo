import { Component, OnInit } from '@angular/core';
import {SenhaModel} from "../models/SenhaModel";
import {SenhaService} from "../services/senha-service.service";
import {GerenteService} from "../services/gerente.service";
import {AlertController} from "@ionic/angular";
import {WebSocketConnector} from "../services/websockets.service";
import {environment} from "../../environments/environment";

@Component({
  selector: 'app-gerente',
  templateUrl: './gerente.page.html',
  styleUrls: ['./gerente.page.scss'],
})
export class GerentePage implements OnInit {
  filaSenhas: SenhaModel[] = [];
  senhaAtiva: SenhaModel = new SenhaModel();
  senhasAtivas: SenhaModel[] = [];

  private webSocketConnector: WebSocketConnector;

  constructor(private gerenteService: GerenteService, private senhaService: SenhaService, private alertController: AlertController ) { }

  ngOnInit() {
    this.refreshSenhas();
    this.webSocketConnector = new WebSocketConnector(
      environment.websocket,
      '/statusProcessor',
      this.onMessage.bind(this)
    );
  }

  chamarProximaSenha(){
    this.gerenteService.chamarProximaSenha()
      .subscribe(res => {
        console.log(res);
        this.buscarSenhasEmAtendimento();
        //this.senhaAtiva = res;
        this.getListaPublica();
        this.buscarSenhasEmAtendimento();
      },error => {
        this.exibeAlertaFilaVazia();
      });
  }

  cancelarSenha(senha){
    this.senhaService.cancelarSenha(senha)
      .subscribe(res => {
        console.log(res);
        this.refreshSenhas();
      });
  }

  buscarSenhasEmAtendimento(){
    this.gerenteService.buscarSenhasEmAtendimento()
      .subscribe(res => {
        this.senhasAtivas = res;
        console.log(res);
      });
  }

  finalizaAtendimento(senha: SenhaModel) {
    this.gerenteService.finalizaAtendimento(senha)
      .subscribe(res => {
        console.log(res);
        this.exibeAlertaFinalizacaoAtendimento();
      });
  }

  limparFila(){
    this.gerenteService.limparFila()
         .subscribe(res => {
           console.log(res);
           this.getListaPublica();
           this.refreshSenhas();
         });
  }

  zerarSenha(){
    this.gerenteService.zerarSenha()
      .subscribe(res => {
        console.log(res);
        this.refreshSenhas();
      });
  }

  getListaPublica(){
    this.senhaService.getListaPublica()
      .subscribe(res =>{
        this.filaSenhas = res;
      });
  }

  refreshSenhas(){
    this.getListaPublica();
    this.buscarSenhasEmAtendimento();
  }

  /** RECEBE MENSAGENS VIA SOCKET
   *  PARA PROCESSAMENTO
   * */
  onMessage(message: any): void {
    console.log('MENSAGEM: ' + message.body);

    if(message.body === 'NOVA_SENHA'){
      this.getListaPublica();
    }else if(message.body === 'CANCELAMENTO_SENHA'){
      this.getListaPublica();
      this.buscarSenhasEmAtendimento();
    }


  }

  /**
   * FUNÇÕES DE UI
   * */
  async exibeAlertaLimparFila() {
    const alert = await this.alertController.create({
      header: 'Atenção!',
      message: 'Confirmar a <strong>limpeza</strong> da fila? Todas as senhas serão canceladas!',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Cancelado...');
          }
        }, {
          text: 'Confirmar!',
          handler: () => {
            this.limparFila();
            console.log('Limpando Fila');
          }
        }
      ]
    });
    await alert.present();
  }

  async exibeAlertaZerarSenha() {
    const alert = await this.alertController.create({
      header: 'Atenção!',
      message: 'A contagem das senhas deve ser reiniciada?',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Cancelado...');
          }
        }, {
          text: 'Sim!',
          handler: () => {
            this.zerarSenha();
            console.log('Zerando senha');
          }
        }
      ]
    });
    await alert.present();
  }


  async exibeAlertaFinalizacaoAtendimento() {
    const alert = await this.alertController.create({
      header: 'Atenção!',
      message: 'Chamar a proxima senha?',
      buttons: [
        {
          text: 'Não',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('cancelado');
            this.buscarSenhasEmAtendimento();
          }
        }, {
          text: 'Chamar!',
          handler: () => {
            this.chamarProximaSenha();
            console.log('Chamando proximo');
          }
        }
      ]
    });
    await alert.present();
  }

  async exibeAlertaFilaVazia() {
    const alert = await this.alertController.create({
      header: 'Oba!',
      message: 'Fila vazia!',
      buttons: ['OK']
    });

    await alert.present();

  }

}
