import { Component, OnInit } from '@angular/core';
import {SenhaModel} from "../models/SenhaModel";
import {SenhaService} from "../services/senha-service.service";
import {WebSocketConnector} from "../services/websockets.service";
import {environment} from "../../environments/environment";

@Component({
  selector: 'app-painel',
  templateUrl: './painel.page.html',
  styleUrls: ['./painel.page.scss'],
})
export class PainelPage implements OnInit {
  filaSenhas: SenhaModel[] = [];
  senhaEmAtendimento: SenhaModel = new SenhaModel();

  playSound: boolean = false;

  private webSocketConnector: WebSocketConnector;

  constructor(private senhaService: SenhaService) {

  }

  ngOnInit() {
    this.getListaPublica();
    this.getSenhaEmAtentimento();
    this.webSocketConnector = new WebSocketConnector(
      environment.websocket,
      '/statusProcessor',
      this.onMessage.bind(this)
    );
  }

  getSenhaEmAtentimento(){
    this.senhaService.getSenhaEmAtendimento()
      .subscribe(res => {
        if(res != null){
          this.senhaEmAtendimento = res;
        }
      });
  }

  getListaPublica(){
    this.senhaService.getListaPublica()
      .subscribe(res =>{
        this.filaSenhas = res;
      });
  }

  playNotificacao(){
    if(this.playSound) {
      let audio = new Audio();
      audio.src = 'assets/notification.mp3';
      audio.load();
      audio.play();
    }
  }

  /** RECEBE MENSAGENS VIA SOCKET*/
  onMessage(message: any): void {
    if(message.body === 'PROXIMA_SENHA'
        || message.body === 'LIMPAR_FILA'){

      this.getSenhaEmAtentimento();
      this.getListaPublica();
      this.playNotificacao();
    }else if(message.body === 'NOVA_SENHA'){
      this.getListaPublica();
    }

  }

  changeSoundStatus(){
    this.playSound = !this.playSound;
  }


}
