import { Injectable } from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient} from "@angular/common/http";
import {SenhaModel} from "../models/SenhaModel";
import {ClienteModel} from "../models/ClienteModel";
import {StorageService} from "./storage.service";
import {NovaSenhaDTO} from "../models/dto/NovaSenhaDTO";
import {LoadingController} from "@ionic/angular";

@Injectable({
  providedIn: 'root'
})
export class ClienteService{
  endpoint = environment.api + 'cliente/';
  clienteAtivo: ClienteModel = new ClienteModel();


  constructor(private httpClient:  HttpClient,
              private storageService: StorageService,
              public loadingController: LoadingController
  ){
    this.novoCliente();
  }

  getClienteSenhasAtivas(){
    return this.httpClient.get<SenhaModel[]>(this.endpoint + this.clienteAtivo.id +'/senhas_ativas');
  }

  novaSenha(nova: NovaSenhaDTO){
    nova.clienteId = this.clienteAtivo.id;
    console.log(nova);
    return this.httpClient.post(this.endpoint + 'senha', nova);
  }

  deleteSenha(senha){
    return this.httpClient.delete(this.endpoint + 'senha/'+ senha.id);
  }

  novoCliente(){
    this.exibeLoading();
    if(this.storageService.getCliente() !== null){
      this.clienteAtivo = this.storageService.getCliente();
      setTimeout(out =>{
        this.loadingController.dismiss();
      },1500);
    }else{
      this.httpClient.post<ClienteModel>(this.endpoint, this.clienteAtivo)
        .subscribe(res => {
          this.storageService.setCliente(res);
          this.clienteAtivo = res;
          setTimeout(out =>{
               this.loadingController.dismiss();
          },1200);
        });
    }
  }

  /**
   * Funções de UI
   * */
  async exibeLoading() {
    const loading = await this.loadingController.create({
      message: 'Aguarde...',
      duration: 3000
    });
    await loading.present();

  }


}
