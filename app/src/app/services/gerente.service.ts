import { Injectable } from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient} from "@angular/common/http";
import {SenhaModel} from "../models/SenhaModel";
import {ClienteModel} from "../models/ClienteModel";
import {StorageService} from "./storage.service";
import {NovaSenhaDTO} from "../models/dto/NovaSenhaDTO";
import {LoadingController} from "@ionic/angular";

@Injectable({
  providedIn: 'root'
})
export class GerenteService{
  endpoint = environment.api + 'gerente/';

  constructor(private httpClient:  HttpClient){

  }

  limparFila(){
    return this.httpClient.post(this.endpoint + 'limpar_fila',null);
  }

  zerarSenha(){
    return this.httpClient.post(this.endpoint + 'senhas/zerar',null);
  }

  chamarProximaSenha(){
    return this.httpClient.post<SenhaModel>(this.endpoint + 'proxima_senha',null);
  }

  finalizaAtendimento(senha: SenhaModel){
     return this.httpClient.post<SenhaModel>(this.endpoint + 'finaliza_atendimento', senha);
  }

  buscarSenhasEmAtendimento(){
    return this.httpClient.get<SenhaModel[]>(this.endpoint + 'senhas/em_atendimento');
  }
}
