import { Injectable } from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient} from "@angular/common/http";
import {SenhaModel} from "../models/SenhaModel";

@Injectable({
  providedIn: 'root'
})
export class SenhaService{
  endpoint = environment.api + 'senha/';

  constructor( private httpClient:  HttpClient){

  }

  getListaPublica(){
     return  this.httpClient.get<SenhaModel[]>(this.endpoint+"lista_publica");
  }

  getSenhaEmAtendimento(){
    return  this.httpClient.get<SenhaModel>(this.endpoint+"em_atendimento");
  }

  cancelarSenha(senha: SenhaModel){
    return  this.httpClient.delete<any>(this.endpoint + senha.id);
  }


}
