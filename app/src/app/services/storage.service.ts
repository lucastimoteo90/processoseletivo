import { Injectable } from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient} from "@angular/common/http";
import {SenhaModel} from "../models/SenhaModel";
import {ClienteModel} from "../models/ClienteModel";

@Injectable({
  providedIn: 'root'
})
export class StorageService {
  constructor(){
  }

  getCliente(){
    return JSON.parse(localStorage.getItem('cliente'));
  }

  setCliente(data){
    localStorage.setItem('cliente', JSON.stringify(data));
  }



}
