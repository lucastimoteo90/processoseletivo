import {Stomp} from "@stomp/stompjs";
import * as SockJS from 'sockjs-client';
export class WebSocketConnector {

  private stompClient: any;

  constructor(private webSocketEndPoint: string, private topic: string, private onMessage: Function, private callbackError?: Function) {
    const errorCallback = callbackError || this.onError;
    this.connect(errorCallback);
  }

  private connect(errorCallback: Function) {
    console.log("INICIANDO CONEXAO");
    const ws = new SockJS(this.webSocketEndPoint);
    this.stompClient = Stomp.over(ws);
    this.stompClient.connect({}, frame => {
      this.stompClient.subscribe(this.topic, event => {
        this.onMessage(event);
      });
    }, errorCallback.bind(this));
  };

  private onError(error) {
    console.log("Falha na conexão: " + error);
    setTimeout(() => {
      console.log("Tentando conectar...");
      this.connect(this.onError);
    }, 3000);
  }
}
