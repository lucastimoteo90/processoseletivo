import {Component, OnInit} from '@angular/core';
import {SenhaService} from "../services/senha-service.service";
import {SenhaModel} from "../models/SenhaModel";
import {ClienteService} from "../services/cliente.service";
import {NovaSenhaDTO} from "../models/dto/NovaSenhaDTO";
import {WebSocketConnector} from "../services/websockets.service";
import {environment} from "../../environments/environment";
import {LoadingController} from "@ionic/angular";

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page implements OnInit{
  filaSenhas: SenhaModel[] = [];
  clienteSenhas: SenhaModel[] = [];

  private webSocketConnector: WebSocketConnector;

  constructor(
    private senhaService: SenhaService,
    public clienteService: ClienteService,
    public loadingController: LoadingController
  ) {}

  ngOnInit(): void {
    this.getClienteSenhas();
    this.getListaPublica();


    this.webSocketConnector = new WebSocketConnector(
      environment.websocket,
      '/statusProcessor',
       this.onMessage.bind(this)
    );
  }

  /** RECEBE MENSAGENS VIA SOCKET*/
  onMessage(message: any): void {
    console.log('MENSAGEM: ' + message.body);
    this.getClienteSenhas();
    this.getListaPublica();
  }
  /**
   * PREFERENCIAL(0,"PREFERENCIAL"),
   * NORMAL(1,"NORMAL");
   * */
  novaSenhaNormal(){
    let novaSenha: NovaSenhaDTO = new NovaSenhaDTO();
    novaSenha.tipo = 1;
    this.exibeLoading();
    this.clienteService.novaSenha(novaSenha)
      .subscribe(res => {
        console.log(res);
        this.getClienteSenhas();
        this.getListaPublica();
      });
  }

  novaSenhaPrioritario(){
    let novaSenha: NovaSenhaDTO = new NovaSenhaDTO();
    novaSenha.tipo = 0;
    this.exibeLoading();
    this.clienteService.novaSenha(novaSenha)
      .subscribe(res => {
        this.getClienteSenhas();
        this.getListaPublica();
      });
  }

  deleteClienteSenha(senha){
    this.clienteService.deleteSenha(senha)
      .subscribe(res => {
         this.getClienteSenhas();
         this.getListaPublica();
      });
  }

  getClienteSenhas(){
    this.clienteService.getClienteSenhasAtivas()
      .subscribe(res => {
        this.clienteSenhas = res;
        for(let s of this.clienteSenhas){
          if(s.status === 'EM_ATENDIMENTO'){
            this.playNotificacao();
          }
        }
      });
  }

  getListaPublica(){
    this.senhaService.getListaPublica()
      .subscribe(res =>{
        this.filaSenhas = res;
      });
  }

  /**
   * Audio de notificaçao*
   **/

  playNotificacao(){
   let audio = new Audio();
   audio.src = 'assets/notification.mp3';
   audio.load();
   audio.play();
  }

  /**
   * Funções de UI
   * */
  async exibeLoading() {
    const loading = await this.loadingController.create({
      message: 'Aguarde...',
      duration: 1500
    });
    await loading.present();
  }

}
