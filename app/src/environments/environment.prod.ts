export const environment = {
  production: true,
  api: 'http://34.69.45.8:8080/',
  websocket: 'http://34.69.45.8:8080/socket'
};
