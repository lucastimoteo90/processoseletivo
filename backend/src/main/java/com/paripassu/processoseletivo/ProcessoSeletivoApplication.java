package com.paripassu.processoseletivo;

import com.paripassu.processoseletivo.domain.Cliente;
import com.paripassu.processoseletivo.domain.Gerente;
import com.paripassu.processoseletivo.domain.Senha;
import com.paripassu.processoseletivo.domain.enums.StatusSenha;
import com.paripassu.processoseletivo.domain.enums.TipoSenha;
import com.paripassu.processoseletivo.services.ClienteService;
import com.paripassu.processoseletivo.services.SenhaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.time.LocalDate;
import java.util.Date;

@SpringBootApplication
public class ProcessoSeletivoApplication implements CommandLineRunner {

    @Autowired
    private ClienteService clienteService;
    @Autowired
    private SenhaService senhaService;


    public static void main(String[] args) {
        SpringApplication.run(ProcessoSeletivoApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {



    }
}
