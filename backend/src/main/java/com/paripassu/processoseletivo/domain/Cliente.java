package com.paripassu.processoseletivo.domain;

import com.paripassu.processoseletivo.domain.enums.StatusSenha;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Cliente implements Serializable {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String nome;
    private String email;
    private String telefone;


    @OneToMany(mappedBy = "cliente")
    private List<Senha> senhas = new ArrayList<Senha>();

    public Cliente(){
    }

    public Cliente(Integer id, String name, String email, String telefone) {
        super();
        this.id = id;
        this.nome = nome;
        this.email = email;
        this.telefone = telefone;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return nome;
    }

    public void setName(String name) {
        this.nome = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public List<Senha> getSenhas() {
        return senhas;
    }

    public List<Senha> getSenhasAtivas(){
        List<Senha> list = new ArrayList<Senha>();
        for(Senha senha : this.getSenhas()){
          if( (senha.getStatus() == StatusSenha.AGUARDANDO)
                  || (senha.getStatus() == StatusSenha.EM_ATENDIMENTO)  ){
              list.add(senha);
          }
        }
        return list;
    }


    public void setSenhas(List<Senha> senhas) {
        this.senhas = senhas;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Cliente cliente = (Cliente) o;

        return id.equals(cliente.id);
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }
}
