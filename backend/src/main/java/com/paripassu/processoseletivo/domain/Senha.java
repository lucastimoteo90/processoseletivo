package com.paripassu.processoseletivo.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.paripassu.processoseletivo.domain.enums.StatusSenha;
import com.paripassu.processoseletivo.domain.enums.TipoSenha;
import org.apache.tomcat.jni.Local;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.Objects;

@Entity
public class Senha implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private Integer posicao;
    private Integer tipo;
    private Integer status;
    private LocalDateTime dataSolicitacao;

    @ManyToOne
    @JoinColumn(name="cliente_id")
    private Cliente cliente;

    @ManyToOne
    @JoinColumn(name="gerente_id")
    private Gerente gerente;

    @Transient
    private Integer cliente_id;

    public Senha(Integer id, TipoSenha tipo, StatusSenha status, Cliente cliente, Gerente gerente) {
        this.id = id;
        this.tipo = tipo.getId();
        this.status = status.getId();
        this.dataSolicitacao = LocalDateTime.now();
        this.cliente = cliente;
        this.gerente = gerente;
    }

    public Senha() {
        this.dataSolicitacao = LocalDateTime.now();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public LocalDateTime getDataSolicitacao() {
        return dataSolicitacao;
    }

    public void setDataSolicitacao(LocalDateTime dataSolicitacao) {
        this.dataSolicitacao = dataSolicitacao;
    }

    public TipoSenha getTipo() {
        return TipoSenha.toEnum(tipo);
    }

    public void setTipo(TipoSenha tipo) {
        this.tipo = tipo.getId();
    }

    public StatusSenha getStatus() {
        return StatusSenha.toEnum(status);
    }

    public void setStatus(StatusSenha status) {
        this.status = status.getId();
    }

    @JsonIgnore
    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Integer getPosicao() {
        return posicao;
    }

    public void setPosicao(Integer posicao) {
        this.posicao = posicao;
    }


    public Integer getClienteId() {
        return cliente_id;
    }

    public void setClienteId(Integer cliente_id) {
        this.cliente_id = cliente_id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Senha senha = (Senha) o;

        return id.equals(senha.id);
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }
}
