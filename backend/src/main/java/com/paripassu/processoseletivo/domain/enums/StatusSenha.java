package com.paripassu.processoseletivo.domain.enums;

public enum StatusSenha {
   AGUARDANDO(0,"AGUARDANDO"),
   EM_ATENDIMENTO(1,"EM_ATENDIMENTO"),
   FINALIZADO(2,"FINALIZADO"),
   CANCELADO(3,"CANCELADO");

   private int id;
   private String descricao;

   private StatusSenha(int id, String descricao){
       this.id = id;
       this.descricao = descricao;
   }

   public static StatusSenha toEnum(Integer id){
       if(id == null){
           return null;
       }

       for(StatusSenha statusSenha : StatusSenha.values()){
           if(id.equals(statusSenha.getId())){
               return statusSenha;
           }
       }

       throw new IllegalArgumentException("ID INVÁLIDO: "+ id);
   }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }
}
