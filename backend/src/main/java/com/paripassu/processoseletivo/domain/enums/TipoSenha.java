package com.paripassu.processoseletivo.domain.enums;

public enum TipoSenha {
   PREFERENCIAL(0,"PREFERENCIAL"),
   NORMAL(1,"NORMAL");

   private int id;
   private String descricao;

   private TipoSenha(int id, String descricao){
       this.id = id;
       this.descricao = descricao;
   }

   public static TipoSenha toEnum(Integer id){
       if(id == null){
           return null;
       }

       for(TipoSenha tipoCliente : TipoSenha.values()){
           if(id.equals(tipoCliente.getId())){
               return tipoCliente;
           }
       }

       throw new IllegalArgumentException("ID INVÁLIDO: "+ id);
   }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }
}
