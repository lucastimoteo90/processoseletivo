package com.paripassu.processoseletivo.dto;

import com.paripassu.processoseletivo.domain.Senha;

public class NovaSenhaDTO {
    private Integer clienteId;
    private Integer tipo;

    public Integer getClienteId() {
        return clienteId;
    }

    public void setClienteId(Integer cliente_id) {
        this.clienteId = cliente_id;
    }

    public Integer getTipo() {
        return tipo;
    }

    public void setTipo(Integer tipo) {
        this.tipo = tipo;
    }
}
