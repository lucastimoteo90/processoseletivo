package com.paripassu.processoseletivo.repositories;

import com.paripassu.processoseletivo.domain.Senha;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface GerenteRepository extends JpaRepository<Senha, Integer> {
   List<Senha> findAllByOrderByTipo();
   List<Senha> findAllByOrderByTipoAscDataSolicitacaoAsc();

}