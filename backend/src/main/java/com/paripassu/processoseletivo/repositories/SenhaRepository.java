package com.paripassu.processoseletivo.repositories;

import com.paripassu.processoseletivo.domain.Senha;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface SenhaRepository extends JpaRepository<Senha, Integer> {
   List<Senha> findAllByStatusOrderByTipoAscDataSolicitacaoAsc(Integer status);
   List<Senha> findAllByStatusInOrderByTipoAscDataSolicitacaoAsc(List<Integer> status);
   Senha findTopByStatusOrderByTipoAscDataSolicitacaoAsc(Integer status);

   List<Senha> findAllByStatus(Integer status);
   Senha findTopByStatus(Integer status);

   List<Senha> findAllBy();

   Senha findTopByOrderByIdDesc();

}