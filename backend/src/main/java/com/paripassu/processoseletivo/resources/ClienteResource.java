package com.paripassu.processoseletivo.resources;

import com.paripassu.processoseletivo.domain.Cliente;
import com.paripassu.processoseletivo.domain.Senha;
import com.paripassu.processoseletivo.dto.NovaSenhaDTO;
import com.paripassu.processoseletivo.services.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(value = "cliente")
public class ClienteResource {

    @Autowired
    private ClienteService clienteService;

    @RequestMapping(value = "/{id}",method = RequestMethod.GET)
    public ResponseEntity<Cliente> find(@PathVariable Integer id){
        return ResponseEntity.ok().body(clienteService.buscar(id));
    }

    @RequestMapping(value = "/{id}/senhas_ativas",method = RequestMethod.GET)
    public ResponseEntity<List<Senha>> getClienteSenhasAtivas(@PathVariable Integer id){
        return ResponseEntity.ok().body(clienteService.buscarSenhasAtivasCliente(id));
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<Cliente> novoCliente(@RequestBody Cliente cliente){
        cliente = clienteService.save(cliente);
        return ResponseEntity.ok().body(cliente);
    }

    @RequestMapping(value = "/senha", method = RequestMethod.POST)
    public ResponseEntity<Senha> novaSenha(@RequestBody NovaSenhaDTO novaSenhaDTO){
        return ResponseEntity.ok().body(clienteService.criarNovaSenha(novaSenhaDTO));
    }

    @RequestMapping(value = "/senha/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Senha> deleteSenha(@PathVariable Integer id){
        return ResponseEntity.ok().body(clienteService.cancelaSenha(id));
    }






}
