package com.paripassu.processoseletivo.resources;

import com.paripassu.processoseletivo.domain.Cliente;
import com.paripassu.processoseletivo.domain.Senha;
import com.paripassu.processoseletivo.services.ClienteService;
import com.paripassu.processoseletivo.services.GerenteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "gerente")
public class GerenteResource {

    @Autowired
    private GerenteService gerenteService;

    @RequestMapping(value = "limpar_fila", method = RequestMethod.POST)
    public ResponseEntity<?> limparFila(){
        gerenteService.limparFila();
        return ResponseEntity.ok().body(null);
    }

    @RequestMapping(value = "proxima_senha", method = RequestMethod.POST)
    public ResponseEntity<Senha> chamarProximaSenha(){
        return ResponseEntity.ok().body(gerenteService.chamarProximaSenha());
    }

    @RequestMapping(value = "finaliza_atendimento", method = RequestMethod.POST)
    public ResponseEntity<Senha> finalizaAtendimento(@RequestBody Senha senha){
        return ResponseEntity.ok().body(gerenteService.finalizaAtendimento(senha));
    }

    @RequestMapping(value = "senhas/em_atendimento", method = RequestMethod.GET)
    public ResponseEntity<List<Senha>> buscarSenhasEmAtendimento(){
        return ResponseEntity.ok().body(gerenteService.buscarSenhasEmAtendimento());
    }

    @RequestMapping(value = "senhas/zerar", method = RequestMethod.POST)
    public ResponseEntity<?> zerarSenha(){
        gerenteService.zerarSenhas();
        return ResponseEntity.ok().body(null);
    }









}
