package com.paripassu.processoseletivo.resources;

import com.paripassu.processoseletivo.domain.Cliente;
import com.paripassu.processoseletivo.domain.Senha;
import com.paripassu.processoseletivo.services.ClienteService;
import com.paripassu.processoseletivo.services.SenhaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "senha")
public class SenhaResource{
    @Autowired
    private SenhaService senhaService;

    @RequestMapping(value = "/lista_publica",method = RequestMethod.GET)
    public ResponseEntity<List<Senha>> listaPublica(){
        return ResponseEntity.ok().body(senhaService.listaPublica());
    }

    @RequestMapping(value = "/em_atendimento",method = RequestMethod.GET)
    public ResponseEntity<Senha> buscarSenhaEmAtendimento(){
        return ResponseEntity.ok().body(senhaService.buscarSenhaEmAtendimento());
    }

    @RequestMapping(value = "/{id}",method = RequestMethod.DELETE)
    public ResponseEntity<?> cancelarSenha(@PathVariable Integer id){
        return ResponseEntity.ok().body(senhaService.cancelarSenha(id));
    }


}
