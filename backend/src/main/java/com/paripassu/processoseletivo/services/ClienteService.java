package com.paripassu.processoseletivo.services;

import com.paripassu.processoseletivo.domain.Cliente;
import com.paripassu.processoseletivo.domain.Senha;
import com.paripassu.processoseletivo.domain.enums.StatusSenha;
import com.paripassu.processoseletivo.domain.enums.TipoSenha;
import com.paripassu.processoseletivo.dto.NovaSenhaDTO;
import com.paripassu.processoseletivo.repositories.ClienteRepository;
import com.paripassu.processoseletivo.repositories.SenhaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ClienteService {

    private SimpMessagingTemplate simpMessagingTemplate;

    @Autowired
    public ClienteService(SimpMessagingTemplate template){this.simpMessagingTemplate = template;}

    @Autowired
    private ClienteRepository clienteRepository;
    @Autowired
    private SenhaRepository senhaRepository;

    @Autowired
    private SenhaService senhaService;


    public Cliente buscar(Integer id){
       return clienteRepository.findById(id).orElse(null);
    }

    public Cliente save(Cliente cliente){
        return clienteRepository.save(cliente);
    }

    public Senha criarNovaSenha(NovaSenhaDTO novaSenhaDTO){
        Cliente cliente = this.buscar(novaSenhaDTO.getClienteId());
        Senha senha = new Senha();
        senha.setCliente(cliente);
        senha.setStatus(StatusSenha.AGUARDANDO);
        senha.setTipo(TipoSenha.toEnum(novaSenhaDTO.getTipo()));

        return senhaService.criarNovaSenha(senha);

    }

    public Senha cancelaSenha(Integer id){
        Senha senha = senhaService.buscar(id); //Obter instancia atualizada
        return senhaService.cancelaSenha(senha);
    }

    public List<Senha> buscarSenhasAtivasCliente(Integer id){
        Cliente cliente = this.buscar(id);
        return cliente.getSenhasAtivas();
    }

}
