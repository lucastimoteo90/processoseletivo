package com.paripassu.processoseletivo.services;

import com.paripassu.processoseletivo.domain.Cliente;
import com.paripassu.processoseletivo.domain.Senha;
import com.paripassu.processoseletivo.domain.enums.StatusSenha;
import com.paripassu.processoseletivo.domain.enums.TipoSenha;
import com.paripassu.processoseletivo.dto.NovaSenhaDTO;
import com.paripassu.processoseletivo.repositories.ClienteRepository;
import com.paripassu.processoseletivo.repositories.SenhaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Scope("singleton")
public class ControleSenhaService {

    private boolean zeraSenha = false;

    public boolean isZeraSenha() {
        return zeraSenha;
    }

    public void setZeraSenha(boolean zeraSenha) {
        this.zeraSenha = zeraSenha;
    }
}
