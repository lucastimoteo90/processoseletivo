package com.paripassu.processoseletivo.services;

import com.paripassu.processoseletivo.domain.Gerente;
import com.paripassu.processoseletivo.domain.Senha;
import com.paripassu.processoseletivo.domain.enums.StatusSenha;
import com.paripassu.processoseletivo.domain.enums.TipoSenha;
import com.paripassu.processoseletivo.repositories.GerenteRepository;
import com.paripassu.processoseletivo.repositories.SenhaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Service
public class GerenteService {
    @Autowired
    private GerenteRepository gerenteRepository;



    @Autowired
    private SenhaService senhaService;

    private SimpMessagingTemplate simpMessagingTemplate;

    @Autowired
    public GerenteService(SimpMessagingTemplate template){this.simpMessagingTemplate = template;}

    public void limparFila(){
        simpMessagingTemplate.convertAndSend("/statusProcessor", "LIMPAR_FILA");
        senhaService.limparFila();
    }

    public void zerarSenhas(){
        senhaService.zeraSenha();
    }


    public Senha chamarProximaSenha(){
        return senhaService.chamarProximaSenha();
    }

    public Senha finalizaAtendimento(Senha senha){
        return senhaService.finalizaAtendimento(senha);
    }

    public List<Senha> buscarSenhasEmAtendimento(){
        return senhaService.buscarSenhasEmAtendimento();
    }






}
