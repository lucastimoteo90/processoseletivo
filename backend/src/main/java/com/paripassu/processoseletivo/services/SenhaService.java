package com.paripassu.processoseletivo.services;

import com.paripassu.processoseletivo.domain.Senha;
import com.paripassu.processoseletivo.domain.enums.StatusSenha;
import com.paripassu.processoseletivo.repositories.SenhaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
public class SenhaService {
    private SimpMessagingTemplate simpMessagingTemplate;

    @Autowired
    ControleSenhaService controleSenhaService;

    @Autowired
    public SenhaService(SimpMessagingTemplate template){this.simpMessagingTemplate = template;}

    @Autowired
    private SenhaRepository senhaRepository;

    public Senha buscar(Integer id){
        return senhaRepository.findById(id).orElse(null);
    }

    public Senha criarNovaSenha(Senha senha){
        //Define a posição da nova senha, recupera ultima posição e adiciona 1
        if(controleSenhaService.isZeraSenha()){
            controleSenhaService.setZeraSenha(false);
            senha.setPosicao(1);
        }else {
            senha.setPosicao((senhaRepository.findTopByOrderByIdDesc() == null) ? 1 : senhaRepository.findTopByOrderByIdDesc().getPosicao() + 1);
        }
        senhaRepository.save(senha);
        simpMessagingTemplate.convertAndSend("/statusProcessor", "NOVA_SENHA");
        return senha;
    }

    public List<Senha> listaPublica(){
        //return senhaRepository.findAllByStatusOrderByTipoAscDataSolicitacaoAsc(StatusSenha.AGUARDANDO.getId());
        List<Integer> status = new ArrayList<Integer>();
        status.add(StatusSenha.EM_ATENDIMENTO.getId());
        status.add(StatusSenha.AGUARDANDO.getId());
        return senhaRepository.findAllByStatusInOrderByTipoAscDataSolicitacaoAsc(status);
    }

    public Senha cancelaSenha(Senha senha){
        simpMessagingTemplate.convertAndSend("/statusProcessor", "CANCELAMENTO_SENHA");
        senha.setStatus(StatusSenha.CANCELADO);
        return senhaRepository.save(senha);
    }

    public List<Senha> buscarSenhasEmAtendimento(){
        return senhaRepository.findAllByStatus(StatusSenha.EM_ATENDIMENTO.getId());
    }


    public Senha chamarProximaSenha(){
      //CHAMAR SENHA COM STATUS 'EM_ATENDIMENTO' SE JA EXISTIR ATENDIMENTO INICIADO
      Senha proxima = senhaRepository.findTopByStatusOrderByTipoAscDataSolicitacaoAsc(StatusSenha.EM_ATENDIMENTO.getId());
      if(proxima != null){
          simpMessagingTemplate.convertAndSend("/statusProcessor", "PROXIMA_SENHA");//NOTIFICAÇÃO VIA SOCKET
          return proxima;
      }

      proxima = senhaRepository.findTopByStatusOrderByTipoAscDataSolicitacaoAsc(StatusSenha.AGUARDANDO.getId());
      proxima.setStatus(StatusSenha.EM_ATENDIMENTO);
      proxima = senhaRepository.save(proxima);
      simpMessagingTemplate.convertAndSend("/statusProcessor", "PROXIMA_SENHA");//NOTIFICAÇÃO VIA SOCKET
      return proxima;
    }

    public Senha finalizaAtendimento(Senha senha){
        senha.setStatus(StatusSenha.FINALIZADO);
        senha = senhaRepository.save(senha);
        simpMessagingTemplate.convertAndSend("/statusProcessor", "FINALIZA_ATENDIMENTO");
        return senha;
    }

    public boolean limparFila(){
        List<Senha> senhas = senhaRepository.findAllBy();

        for(Senha senha: senhas){
            if(senha.getStatus().equals(StatusSenha.AGUARDANDO)
               || senha.getStatus().equals(StatusSenha.EM_ATENDIMENTO)){
                senha.setStatus(StatusSenha.CANCELADO);
                senha.setPosicao(0);
                senhaRepository.save(senha);
            }
        }

        Senha last_register = senhaRepository.findTopByOrderByIdDesc();
        last_register.setPosicao(0); //GARANTIR RESTART DA FILA;
        senhaRepository.save(last_register);

        return true;
    }

    public Senha buscarSenhaEmAtendimento(){
        return senhaRepository.findTopByStatus(StatusSenha.EM_ATENDIMENTO.getId());
    }

    public boolean cancelarSenha(Integer id){
        senhaRepository.deleteById(id);
        return true;
    }

    public void zeraSenha(){
        controleSenhaService.setZeraSenha(true);
    }


}
